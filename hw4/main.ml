
(* Modify only interpret and typecheck *)

open Ast

exception ListError
exception TypeError
exception RunTimeError
exception Unimplemented (* shows you what to change *)

(******* environment, context, and heap implementation *******)
(* note: using mutation and a "fresh" function would make some things
         more efficient, but that is not our focus *)
(******* environment, context, and heap implementation *******)
(* note: using mutation and a "fresh" function would make some things
         more efficient, but that is not our focus *)
let empty = []
 
let extend lst x e = (x, e) :: lst
 
let rec lookup lst y =
  match lst with
  | [] -> raise ListError
  | (x, v) :: tl -> if x = y then v else lookup tl y
  
let rec update lst y v =
  match lst with
  | [] -> raise ListError
  | (x, v2) :: tl ->
      if x = y then (x, v) :: tl else (x, v2) :: (update tl y v)
  
(* assumes lst was built only by previous calls to extend_fresh and update *)
let extend_fresh lst e =
  let newlab = "l" ^ (string_of_int ((List.length lst) + 1))
  in (newlab, ((newlab, e) :: lst))
  
(* Do not change anything above here. *)

(************* interpreter ***************)
(* note: remember we are large-step -- return a heap and a value *)
(* note: several cases are done for you; implement the rest *)
(* The remaining cases are: *)
(*   Times(e1,e2), GreaterThan(e1,e2), Var x, Get e, and Set(e1, e2) *)

let rec interpret heap env e = 
  let binop f heap env e1 e2 =
    let (v1, h1) = interpret heap env e1 in
    let (v2, h2) = interpret h1 env e2
    in
      match (v1, v2) with
      | (Const i1, Const i2) -> ((Const (f i1 i2)), h2) 
      | _ -> raise RunTimeError
  in
    match e with
    | Const i -> (e, heap)
    | Plus (e1, e2) -> binop (fun x y -> x + y) heap env e1 e2 
    | Times (e1, e2) -> binop (fun x y -> x * y) heap env e1 e2
    | GreaterThan (e1, e2) -> 
			let (v1, h1) = interpret heap env e1 in
    	let (v2, h2) = interpret h1 env e2 in
      (match (v1, v2) with
         | (Const i1, Const i2) -> (if i1 > i2 then Const(1) else Const(0)), h2
         | _ -> raise RunTimeError)
    | If (e1, e2, e3) ->
        let (v1, h1) = interpret heap env e1
        in
          (match v1 with
           | Const 0 -> interpret h1 env e3
           | Const _ -> interpret h1 env e2
           | _ -> raise RunTimeError)
    | Var x -> (try lookup env x with | ListError -> raise TypeError), heap
    | Fun (_, _, _) -> ((Closure (e, env)), heap)
    | Apply (e1, e2) ->
        let (v1, h1) = interpret heap env e1 in
        let (v2, h2) = interpret h1 env e2
        in
          (match v1 with
           | Closure ((Fun (x, _, e3)), envc) -> 
               interpret h2 (extend envc x v2) e3
           | _ -> raise RunTimeError)
    | Closure (_, _) -> (e, heap)
    | Get e -> 
			let (v1, h1) = interpret heap env e in
			(match (v1, h1) with
      | (Label(l), h1) -> lookup h1 l, h1
			| _ -> raise RunTimeError)
    | Set (e1, e2) -> 
			let (v1, h1) = interpret heap env e1 in
			(match (v1, h1) with
			| (Label(l), h1) -> 
				let (v2, h2) = interpret h1 env e2 in 
				v2, update h2 l v2
			| _ -> raise RunTimeError)
    | Alloc e -> 
        let (v1, h1) = interpret heap env e in
        let (l, h2) = extend_fresh h1 v1 in ((Label l), h2)
    | Label l -> (e, heap)



(************** type-checker ***********)
(* note: t1=t2 will do what you want to compare types structurally, 
   e.g., Arrow(Int,Int)=Arrow(Int,Int), which stands for int->int = int->int
   But t1==t2 is roughly pointer-equality -- do not accidentally use it or
   you will be very sorry *)
(* note: a few cases are done for you *)
(* The remaining cases are: *)
(*   Times(e1,e2), GreaterThan(e1,e2), Apply(e1,e2), and Set(e1, e2) *)

let rec typecheck ctxt e =
  let tcbinop ctxt e1 e2 =
    let t1 = typecheck ctxt e1 in
    let t2 = typecheck ctxt e2
    in if (t1 = Int) && (t2 = Int) then Int else raise TypeError
  in
    match e with
    | Const i -> Int
    | Plus (e1, e2) -> tcbinop ctxt e1 e2
    | Times (e1, e2) -> tcbinop ctxt e1 e2
    | GreaterThan (e1, e2) -> tcbinop ctxt e1 e2
    | If (e1, e2, e3) ->
        let t1 = typecheck ctxt e1 in
        let t2 = typecheck ctxt e2 in
        let t3 = typecheck ctxt e3
        in if (t1 = Int) && (t2 = t3) then t2 else raise TypeError
    | Var x -> (try lookup ctxt x with | ListError -> raise TypeError)
    | Fun (x, t, e) ->
        let t1 = typecheck (extend ctxt x t) e in Arrow (t, t1)
    | Apply (e1, e2) -> 
				let t1 = typecheck ctxt e1 in
   		 	let t2 = typecheck ctxt e2 in
    		(match t1 with 
       		Arrow(t3,t4) -> if t3 = t2 then t4 
					else raise TypeError
     			| _ -> raise TypeError)
    | Closure (_, _) -> raise TypeError
    | Get e ->
        (match typecheck ctxt e with | Ref t -> t | _ -> raise TypeError)
    | Set (e1, e2) -> 
				let t1 = typecheck ctxt e1 in
   		  let t2 = typecheck ctxt e2 in
   		 (match t1 with
       	Ref(t3) -> if t3 = t2 then t2 
				else raise TypeError
     		| _ -> raise TypeError)
    | Alloc e -> Ref (typecheck ctxt e)
    | Label _ -> raise TypeError


(* Do not change anything below here *)

(* expect one command-line argument, a file to parse *)
(* you do not need to understand this interaction with the system *)
let parse_file () =
  let argv = Sys.argv in
  let _ =
    if ( != ) (Array.length argv) 2
    then
      (prerr_string ("usage: " ^ (argv.(0) ^ " [file-to-parse]\n")); exit 1)
    else () in
  let ch = open_in argv.(1)
  in Parse.program Lex.lexer (Lexing.from_channel ch)

let _ =
  let prog = parse_file ()
  in
    try
       (*print_string (exp2string prog);*) 
      (* useful for debugging *)
      (ignore (typecheck empty prog);
       print_string "\ntypechecking complete";
       flush stdout;
       print_string "\ninterpreted answer = ";
       print_string
         (exp2string ((fun (x, _) -> x) (interpret empty empty prog)));
       print_newline ())
    with | TypeError -> print_string "did not typecheck\n"
  
