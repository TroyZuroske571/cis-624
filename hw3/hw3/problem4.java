package hw3;



/**
 * Implmentation of a combinator function and use it to compute the Ackermann 
 * function recursively. The Ackermann function is defined as follows.
 * A(0, n) = n + 1
 * A(m + 1, 0) = A(m, 1)
 * A(m + 1, n + 1) = A(m, A(m + 1, n)).
 *
 * Y-combinator code from: http://rosettacode.org/wiki/Y_combinator#Java
 *
 * @author  Troy Zuroske, University of Oregon
 */

public class problem4 
{
	interface Ack {long apply(long m, long n);}
	
	static class AckermanHelper implements Ack 
    {
        @SuppressWarnings("unused")
		private final Ack f;
        public AckermanHelper(Ack f) 
        {
            this.f = f;
        }

        public long apply(long m, long n) 
        {
        	if (m == 0) 
        	{
                return n + 1;
            } else if (n == 0) 
            {
                return apply(m-1, 1); 
            } else 
            {            
                return apply(m-1, apply(m,n-1));
            }
        }
    }
	
	interface FunctToFunct {Ack apply(Ack f);}
	
	static class AckermannGen implements FunctToFunct 
	{
        public Ack apply(Ack f) 
        {
            return new AckermanHelper(f);
        }
    }
	
	static class YCombinator implements Ack 
	{
        private final FunctToFunct mFunctToFunct;

        public YCombinator(FunctToFunct intFuncToIntFunc)
        {
            mFunctToFunct = intFuncToIntFunc;
        }
        
        public Ack fixedPoint() 
        {
            return mFunctToFunct.apply(this);
        }
        
        public long apply(long m, long n) 
        {     
            final Ack func = mFunctToFunct.apply(this);
            return func.apply(m, n);
        }
    }

    public static void main(String args[]) 
    {     
    	long i = Long.parseLong(args[0]);
    	long j = Long.parseLong(args[1]);
        final FunctToFunct ackerGen = new AckermannGen();
        final Ack ackerman = new YCombinator(ackerGen).fixedPoint();

        if (i < 0 || j < 0) 
        {
        	System.out.println("Nonnegative numbers only.");
        }
        
        System.out.println(ackerman.apply(i, j));
    }
}
