(* This file is the only one you should modify *)
open Ast

exception Unimplemented

(* a little warm-up *)
let rec string_of_valu v = 
	match v with
	| VConst (i) -> string_of_int i
	| VPair(v, v2) -> "(" ^ string_of_valu v ^ "," 
													^ string_of_valu v2 ^ ")"
	| VTagged(str,v) -> str ^ "(" ^ string_of_valu v ^ ")"

let rec string_of_binding_list lst = 
	match lst with
	| [] -> ""
	| (str, v)::lstEnd -> "\n" ^ str ^ ": " 
	^ string_of_valu v 
	^ string_of_binding_list lstEnd

(* a little helper function you may find useful; you don't have to use it *)
let orelse opt thunk =
  match opt with
    Some x -> opt
  | None -> thunk()

let rec large p v =
  match p, v with
    PWild,  _ -> Some []
  | PVar s, _ -> Some [(s,v)]
  | PConst i1, VConst i2 -> if i1 = i2 then Some [] else None
  | PPair(p1,p2), VPair(v1,v2) -> 
    let b1 = large p1 v1 in 
    let b2 = large p2 v2 in
    (match (b1,b2) with
       (Some e1, Some e2) -> 
				Some (e1 @ e2)
    	 | _ -> None)
  | PTagged(s1,p1), VTagged(s2,v1) -> if s1 = s2 then large p1 v1 else None
  | PDescendent p1, _ ->
      orelse (large p1 v)
        (fun () -> 
          match v with
            VConst _ -> None
          | VPair(v1,v2)  -> orelse (large p v1) (fun () -> large p v2)
          | VTagged(s,v1) -> large p v1)
  | _ -> None

let rec small_step p v b = 
  match p, v with
    PWild, _ -> [(p,v,b)]
  | PVar s, _ -> [(PWild, v, [(s,v)] @ b)]
  | PConst i1, VConst i2 -> if i1 = i2 then [(PWild, v, b)] else []
  | PPair(PWild,p2), VPair(_,v2) -> [(p2,v2,b)]
  | PPair(p1,p2), VPair(v1,v2) -> 
      List.map (fun (p,v,b) -> (PPair(p,p2),VPair(v,v2),b))
        (small_step p1 v1 b)
  | PTagged(s1,p1), VTagged(s2,v1) -> if s1 = s2 then [(p1,v1,b)] else []
  | PDescendent p1, _ ->
      (p1,v,b)::
      (match v with
        VConst _ -> []
      | VPair(v1,v2) -> [(p,v1,b); (p,v2,b)]
      | VTagged(_,v1) -> [(p,v1,b)])
  | _ -> []

let rec iter stack = (* do not change *)
  match stack with
    [] -> None
  | (PWild,_,b)::tl -> Some b
  | (p,v,b)::tl -> iter ((small_step p v b) @ tl)

let small p v = iter [(p, v, [])] (* do not change *)

let rec denote p =
  match p with
    PWild -> (fun v -> Some [])
  | PVar s -> (fun v -> Some [(s,v)])
  | PConst i1 -> fun v -> 
									(match v with 
         						VConst(e) -> Some []
       							| _ -> None)
  | PPair(p1,p2) -> 
      let f1 = denote p1 in
      let f2 = denote p2 in
      (fun v ->
        match v with
          VPair(v1,v2) -> 
           (* wasteful to always recur on both, but a bit cleaner *)
            (match f1 v1, f2 v2 with
              Some b1, Some b2 -> Some (b1 @ b2)
            | _ -> None)
        | _ -> None)
  | PTagged(s1,p1) -> let den = denote p1 in
      fun j ->
        (match j with
           VTagged(s2,v2) -> if s2 = s1 then den v2 else None
         	 | _ -> None)
  | PDescendent p1 ->
			let den = denote p1 in 
			let rec e v =
				match v with 
				| VPair (v1, v2) -> orelse (den v1) (fun () -> den v2)
				| VTagged (s1, v1) -> den v1
				| _ -> None
			in
				fun b -> orelse (e b) (fun () -> den b)

(* do not change *)
let print_ans ans =
  print_string
    (match ans with
      None -> "\nno match"
    | Some b -> "\nmatch: " ^ (string_of_binding_list b))

(* the next two bindings really belong in a "main" module, but this
keeps all the code you need in one place *)

(* expect one command-line argument, a file to parse and interpret *)
(* do not change this *)
let get_prog () =
  let argv = Sys.argv in
  let _ = 
    if Array.length argv != 2
    then (prerr_string ("usage: " ^ argv.(0) ^ " [file-to-interpret]\n");
	  exit 1) in
  let ch = open_in argv.(1) in
  Parse.program Lex.lexer (Lexing.from_channel ch)

(* change this only by uncommenting out parts when you are ready *)
let _ =
  let (p,v) = get_prog () in
  ()
	; print_ans (large p v)
  ; print_ans (denote p v)
  ; print_ans (small p v) 
  ; print_string "\n"
