 
type inttree = Empty | Node of int * inttree * inttree
exception DivisionByZero

(* use this function in fromList *)
let rec insert t i =
  match t with
      Empty -> Node(i,Empty,Empty)
    | Node(j,l,r) -> 
      if i=j 
      then t 
      else if i < j 
      then Node(j,insert l i,r) 
      else Node(j,l,insert r i)

(* no need for this function; it is just an example *)
let rec member t i =
  match t with
      Empty -> false 
    | Node(j,l,r) -> i=j || (i < j && member l i) || member r i

(* put fromList, sum1, prod1, avg1, map and negateAll here *)

(*1a*)
let rec fromList (lst : int list) = 
	match lst with
    [ ] -> Empty
  | head::tail -> insert (fromList tail) head

(*1b*)
let rec sum1 (s : inttree) = 
	match s with
	| Empty -> 0
	| Node (j,l,r) ->
		j + sum1 r + sum1 l

let rec prod1 (s : inttree) = 
	match s with
	| Empty -> 1
	| Node (j,l,r) -> 
		j * prod1 r * prod1 l

let rec avgHelp (s : inttree) = 
	match s with
	| Empty -> (0, 0)
	| Node(j,l,r) -> 
		 let (sumr, ctr) = avgHelp r in
		 let (suml, ctl) = avgHelp l in
		 (j + sumr + suml, 1 + ctr + ctl)
			
let avg1 (s : inttree) = 
	if (snd (avgHelp s)) = 0
	then raise DivisionByZero
	else (fst (avgHelp s)) / (snd (avgHelp s))

(*1c*)
let rec map (f : int -> int) (s : inttree) = 
	match s with
	| Empty -> Empty
	| Node (j,l,r) -> 
		Node ((f j), (map (f : int -> int) l), (map (f : int -> int) r))

(*1d*)
let negateAll (s : inttree) = (map (fun (x : int) -> -x) s)
 
let rec fold f a t =
  match t with
      Empty -> a
    | Node(j,l,r) -> fold f (fold f (f a j) l) r


(* put sum2, prod2, and avg2 here *)

(*1e*)
let sum2 (s : inttree) = fold (fun a j -> a + j) 0 s

let prod2 (s : inttree) = fold (fun a j -> a * j) 1 s

let avg2 (s : inttree) = 
  let (x, y) = fold (fun (a,b) c -> (a+c, b+1)) (0,0) s in
  if y == 0 
	then raise DivisionByZero 
	else x / y 

type 'a iterator = Nomore | More of 'a * (unit -> 'a iterator)

let rec iter t =
  let rec f t k =
    match t with
	Empty -> k ()
      | Node(j,l,r) -> More(j, fun () -> f l (fun () -> f r k))
  in f t (fun () -> Nomore)

(*1f*)
(*A client attempting to use the iter function to process all of the ints in 
a tree would call the function on that tree to process each node and return the
type iterator. This iterator type resembles the data structure of a linked list
which the client would then use in a recursive function that would finish when 
(fun () -> Nomore) is the second element. 

The function iter is implemented by producing the data structure: linked list
from a tree. First, the tree's nodes are evaluated depth first from right to
left. The function does this by recursively calling the constructor of the 
iterator that is returned. *)

(*1g*)
let sum3 (s : inttree) =
  let rec loop (x : int) itr =
    match itr with 
    | Nomore -> x
    | More(j, f) -> loop (x + j) (f()) in
  	loop 0 (iter s)  

let prod3 (s : inttree) =
  let rec loop (x : int) itr =
    match itr with 
    | Nomore -> x
    | More(j, f) -> if(j = 0) then 0 else loop (x * j) (f()) in
  	loop 1 (iter s)  

let avg3 (s : inttree) =
  let rec avgLoop itr (i,k) =
    match itr with 
    | Nomore -> (i, k)
    | More(j, f) -> avgLoop (f()) (i + j, k + 1) in
  	let (g,z) = avgLoop (iter s) (0,0) in
    if z = 0 
		then raise DivisionByZero 
		else g / z

(* challenge problem: put optionToException and exceptionToOption here *)

(* a little testing -- commented out since the functions do not exist yet *)

let tr = fromList [0;1;2;3;4;5;6;7;8;9;9;9;1] (* repeats get removed *)
let print_ans f t = print_string (string_of_int (f t)); print_string "\n"

let _ = print_ans sum1 tr
let _ = print_ans prod1 tr
let _ = print_ans avg1 tr
let _ = print_ans sum2 tr
let _ = print_ans prod2 tr
let _ = print_ans avg2 tr
let _ = print_ans sum3 tr
let _ = print_ans prod3 tr
let _ = print_ans avg3 tr
