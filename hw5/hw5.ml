(* CIS 624 Homework 5 *)
(* Created by: Troy Zuroske *)
(* December 4, 2015 *)

(*1.*)
let addk i j k = k(i + j)
let subk i j k = k(i - j)
let timesk i j k = k(i * j)
let plusk i j k = k(i +. j)
let take_awayk i j k = k(i -. j)
let multk i j k = k(i *. j)
let catk i j k = k(i ^ j)
let consk i j k = k(i :: j)
let lessk i j k = k(i < j)
let eqk i j k = k(i = j)

(*2.*)
let abcdk a b c d k =
    multk d a
      (fun da -> multk b c (fun bc -> plusk a bc (fun abc -> plusk abc da k)))

(*3a.*)
let rec fact_range n m = 
	if n < m 
	then 1 
	else n * fact_range (n - 1) m

(*3b.*)
let rec fact_rangek n m k =
    lessk n m
     (fun fac -> if fac 
									then k 1
        					else subk n 1 (fun fac2 -> fact_rangek fac2 m 
																(fun fac3 -> timesk n fac3 k)))

(*4a.*)
let rec app_all flst x =
    match flst with [] -> []
    | (i :: ij) -> 
      let k = (i x) in k :: app_all ij x
					
(*4b.*)
let rec app_allk flstk x k =
    match flstk with
		| [] -> k []
    | (ik :: ikj) -> ik x (fun a1 -> app_allk ikj x (fun a2 -> consk a1 a2 k))

(*5.*)
let rec sum_wholesk l k xk =
    match l with [] -> k 0
    | (i :: ij) ->
      lessk i 0
      (fun a -> if a then xk i else sum_wholesk ij (fun z -> addk i z k) xk)

let report x =
print_string "Result: ";
print_int x;
print_newline();;